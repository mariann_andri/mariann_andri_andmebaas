-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: tap_registry
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `shipments`
--

DROP TABLE IF EXISTS `shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `study_nr` varchar(50) NOT NULL,
  `shipment_nr` int DEFAULT NULL COMMENT 'Mitmes shipent on samale uuringule',
  `order_date` date NOT NULL,
  `number_of_patients` int NOT NULL,
  `number_of_kits` int DEFAULT NULL COMMENT 'Komplektide arv',
  `expiry_date` date DEFAULT NULL COMMENT 'shipmendi säilivusaja lõpp',
  `dispatch_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipments`
--

LOCK TABLES `shipments` WRITE;
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` VALUES (1,'FTXT-TEST1',1,'2020-01-16',5,10,'2021-01-16','1997-01-20'),(2,'FTXT-TEST1',2,'2020-02-16',5,10,'2021-01-18','2020-01-19'),(3,'FTXT-TEST2',1,'2020-01-17',3,15,'2021-01-17','2020-01-18'),(5,'FTXT-TEST2',2,'2020-03-17',3,15,'2021-03-17','2020-03-18'),(12,'FTXT-TEST1',3,'2020-03-19',4,4,'2020-03-27','2020-03-19'),(13,'FTXT-TEST1',4,'2020-03-04',5,5,'2020-03-20','2020-03-27'),(14,'FTXT-TEST2',3,'2020-03-05',4,4,'2020-03-18','2020-03-04');
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studies`
--

DROP TABLE IF EXISTS `studies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `study_nr` varchar(50) NOT NULL,
  `client` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Study_nr_UNIQUE` (`study_nr`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studies`
--

LOCK TABLES `studies` WRITE;
/*!40000 ALTER TABLE `studies` DISABLE KEYS */;
INSERT INTO `studies` VALUES (1,'FTXT-TEST3','BCS'),(2,'FTXT-TEST1','ERR'),(3,'FTXT-TEST2','TV3'),(9,'FTXT-TEST9','BCS'),(11,'FTXT-muudetud','ERR');
/*!40000 ALTER TABLE `studies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taps`
--

DROP TABLE IF EXISTS `taps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `taps` (
  `id` int NOT NULL AUTO_INCREMENT,
  `shipment_id` int NOT NULL,
  `marking` varchar(45) DEFAULT NULL,
  `combination` varchar(45) DEFAULT NULL,
  `tap_nr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `taps in shipment_idx` (`shipment_id`),
  CONSTRAINT `taps in shipment` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taps`
--

LOCK TABLES `taps` WRITE;
/*!40000 ALTER TABLE `taps` DISABLE KEYS */;
INSERT INTO `taps` VALUES (1,1,'1','Tap 1 combination','TAP-1'),(2,1,'1L','Tap 2 combination','TAP-2'),(3,1,'1NL','Tap 3 combination','TAP-3'),(4,2,'2','Tap 10 combination','TAP-1'),(5,2,'2L','Tap 11 combination','TAP-2'),(6,2,'2NL','Tap 12 combination','TAP-3'),(7,2,'3','Tap 13 combination','TAP-4'),(8,2,'3L','Tap 14 combination','TAP-5'),(9,2,'3NL','Tap 15 combination','TAP-6'),(10,3,'1','Tap 1 combination','TAP-1'),(11,3,'1L','Tap 2 combination','TAP-2'),(12,3,'1NL','Tap 3 combination','TAP-3');
/*!40000 ALTER TABLE `taps` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-23 11:35:12
